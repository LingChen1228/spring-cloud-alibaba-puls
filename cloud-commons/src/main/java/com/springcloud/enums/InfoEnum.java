package com.springcloud.enums;

/**
 * @ClassName: InfoEnum
 * @author: Linn
 * @Date: 2019年8月19日20:48:05
 */
public enum InfoEnum {

    /**
     * TODO  重新整理返回码
     */

    //请求正常
    AUTHENTICATION_ERROR(403, "AUTHENTICATION_ERROR", "账号或密码错误,系统鉴权失败"),
    //请求正常
    OK(10000, "OK", "请求正常"),
    //系统忙
    ERROR(0, "ERROR", "系统忙"),
    //无内容
    NO_CONTENT(10001, "NO_CONTENT", "无内容"),
    //错误请求
    BAD_REQUEST(10002, "BAD_REQUEST", "错误请求"),
    //不合法参数
    INVALID_PARAMETERS(10003, "INVALID_PARAMETERS", "不合法参数");


    private String info;
    private String message;
    private int infoCode;

    public String getInfo() {
        return info;
    }

    public String getMessage() {
        return message;
    }

    public int getInfoCode() {
        return infoCode;
    }


    InfoEnum(int infoCode, String info, String message) {
        this.info = info;
        this.infoCode = infoCode;
        this.message = message;
    }

    public static InfoEnum info(int infoCode) {
        for (InfoEnum infoEnum : values()) {
            if (infoEnum.getInfoCode() == infoCode) {
                return infoEnum;
            }
        }
        return null;
    }

}
