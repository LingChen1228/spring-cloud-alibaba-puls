package com.springcloud.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: TokenException
 * @author: Linn
 * @Date: 2019年8月19日21:56:41
 */
@Slf4j
public class TokenException extends RuntimeException{

    public TokenException() {
        super("获取用户信息失败,不允许操作,原因:登录凭证过期");
    }
    public TokenException(String message) {
        super(message);
    }
}
