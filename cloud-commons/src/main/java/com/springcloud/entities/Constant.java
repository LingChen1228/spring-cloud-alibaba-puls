package com.springcloud.entities;

/**
 * @ClassName: Constant
 * @author: Linn
 * @Date: 2019年8月19日21:33:41
 */
public class Constant {
    /**
     * OK
     */
    public static final String OK = "OK";

    /**
     * ERROR
     */
    public static final String ERROR = "ERROR";

}
