package com.springcloud.entities;

import com.springcloud.enums.InfoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashMap;

/**
 * @ClassName: R
 * @author: Linn
 * @Date: 2019年8月19日23:03:31
 */

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class R extends LinkedHashMap<String,Object>{

    private R(boolean flag){
        if (flag){
            put("status",true);
            put("info",Constant.OK);
            put("infoCode", InfoEnum.OK.getInfoCode());
        }else{
            put("status",false);
            put("info",Constant.ERROR);
            put("infoCode",InfoEnum.ERROR.getInfoCode());
        }
    }

    public static R warn(InfoEnum infoEnum) {
        R r = new R(false);
        if(infoEnum!=null){
            r.put("info",infoEnum.getInfo());
            r.put("message",infoEnum.getMessage());
            r.put("infoCode",infoEnum.getInfoCode());
        }
        return r;
    }

    public static R warn(String message) {
        R r = new R(false);
        r.put("message",message);
        return r;
    }

    public static R error() {
        return new R(false);
    }

    public static R ok() {
        return new R(true);
    }

    public static R ok(Object value) {
        R r = new R(true);
        r.addObject(value);
        return r;
    }

    public R add(String key,Object value) {
        put(key,value);
        return this;
    }

    public R addObject(Object value) {
        put("data",value);
        return this;
    }





}


