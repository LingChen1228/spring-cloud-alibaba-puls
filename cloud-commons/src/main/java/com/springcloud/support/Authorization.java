package com.springcloud.support;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: Authorization
 * @author: Linn
 * @Date: 2019年8月19日21:45:45
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Authorization implements Serializable {

    /**
     * 用户id
     */
    private Long id;

    /**
     * 登录账号
     */
    private String username;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 密码
     */
    private String password;
    /**
     * md5密码盐
     */
    private String salt;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 性别(0-默认未知,1-男,2-女)
     */
    private Integer sex;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 状态(0-正常,1-冻结)
     */
    private Boolean status;
    /**
     * 删除状态(0-正常,1-已删除)
     */
    private Boolean delFlag;
    /**
     * 第三方登录的唯一标识
     */
    private String thirdId;
    /**
     * 第三方类型
     */
    private String thirdType;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 身份（1普通成员 2上级）
     */
    private Integer userIdentity;

    /**
     * 登录凭证
     */
    private String token;

}
