package com.springcloud.support;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * @ClassName: HttpHelper
 * @Description: http获取json工具类
 * @author: Linn
 * @Date: 2019年8月19日21:47:11
 */
public class HttpHelper {

    //创建一个无参构造函数
    private HttpHelper(){
        System.out.println("我是HttpHelper的无参构造");
    };

    //指定类初始化日志对象
    private static Logger logger = LoggerFactory.getLogger(HttpHelper.class);


    public static String getJsonString(ServletRequest request) {
        //创建一个StringBuilder对象
        StringBuilder sb = new StringBuilder();
        //初始化一个InputStream对象 InputStream这个抽象类是所有基于字节的输入流的超类，抽象了Java的字节输入模型。
        InputStream inputStream = null;
        //BufferedReader是为了提供读的效率而设计的一个包装类，它可以包装字符流。可以从字符输入流中读取文本，缓冲各个字符，从而实现字符、数组和行的高效读取。
        BufferedReader reader = null;
        try {
            //从request中取出全部请求信息
            inputStream = request.getInputStream();
            //读取信息
            reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            String line;
            //循环读取
            while ((line = reader.readLine()) != null) {
                //读取一条,往sb中插入一条信息
                sb.append(line);
            }
        } catch (IOException e) {
            //读取中报错
            logger.warn("getJsonString出现问题！");
        } finally {
            //无论报错与否 都要执行下面的代码
            //判断inputStream 是否为空
            if (inputStream != null) {
                try {
                    //不为空 关闭读取流资源
                    inputStream.close();
                } catch (IOException e) {
                    //报错 抛出io异常
                    e.printStackTrace();
                }
            }
            //判断从inputStream中读取是否有值
            if (reader != null) {
                try {
                    //有值  关闭流
                    reader.close();
                } catch (IOException e) {
                    //抛出io异常
                    e.printStackTrace();
                }
            }
        }
        //返回在request中读取的信息
        return sb.toString();
    }


    //获取token信息 这里就不详解了...很简单..不懂就去百度
    public static String getToken(ServletRequest request) {
        String jsonString = getJsonString(request);
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        Object token = jsonObject.get("token");
        if(token!=null){
            return token.toString();
        }
        return "";
    }

}
