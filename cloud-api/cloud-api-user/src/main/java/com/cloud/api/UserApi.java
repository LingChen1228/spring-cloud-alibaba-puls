package com.cloud.api;

import com.cloud.service.UserApiService;
import com.springcloud.entities.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserApi {

    @Resource
    UserApiService userApiService;

    @GetMapping(value = "/client/user/nacos/{name}")
    @ResponseBody
    R getUser(@PathVariable("name") String name){
       return userApiService.getUser(name);
    }

}
