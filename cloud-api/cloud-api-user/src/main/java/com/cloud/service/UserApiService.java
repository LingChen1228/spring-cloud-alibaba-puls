package com.cloud.service;

import com.springcloud.entities.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
/** 指定调用服务的服务名 */
@FeignClient(value = "cloud-provider-user")
public interface UserApiService {

    @GetMapping(value = "/user/nacos/{name}")
    R getUser(@PathVariable("name") String name);

}

