package com.springcloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @ClassName: ServerApplication
 * @Author: Linn
 * @Date: 2019年8月18日 21:14:01
 * @Description:添加允许修改请求头操作 主要是解决跨域问题
 */
@Configuration
public class CorsConf {
    /**
     * CORS
     * 规范化的跨域请求解决方案，安全可靠。
     * 优势：
     * 在服务端进行控制是否允许跨域，可自定义规则
     * 支持各种请求方式
     * 缺点：
     * 会产生额外的请求
     * 我们这里会采用cors的跨域方案
     * 详解: https://www.jianshu.com/p/98d4bc7565b2
     *
     * 源码解释
     * 这是Spring MVC Java配置和XML命名空间CORS配置的另一种选择，
     * 适用于只依赖spring-web(不依赖spring-webmvc)或for的应用程序
     * 需要在{@link javax.servlet.Filter}执行CORS检查的安全约束水平。
     * 这个过滤器可以与DelegatingFilterProxy一起使用
     * @Author: Linn
     */
    @Bean
    public CorsFilter corsFilter() {
        //CorsConfiguration基于CorsConfiguration路径模式映射的集合提供每个请求实例。
        //支持精确路径映射URI（例如"/admin"）以及Ant样式路径模式（例如"/admin/**"）
        //这是该类的源码注释
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //允许的域,不要写*，否则cookie就无法使用了
        corsConfiguration.addAllowedOrigin("*");
        //允许的头信息
        corsConfiguration.addAllowedHeader("*");
        //3) 允许的请求方式 *代表所有请求方式
        corsConfiguration.addAllowedMethod("*");
        //添加映射路径，我们拦截一切请求..配置对所有接口都有效
        source.registerCorsConfiguration("/**", corsConfiguration);
        //返回新的CorsFilter
        return new CorsFilter(source);
    }

}
