package com.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName: ServerApplication
 * @Author: Linn
 * @Date: 2019年8月18日 21:14:01
 */
@Configuration
public class RestConfig {

    /**
     * 手动注入一个RestTemplate 加上@LoadBalanced 有负载均衡的能力 自动配置的没有
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }


}
