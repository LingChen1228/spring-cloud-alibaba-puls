package com.springcloud.config;

/**
 * @program: MvcConfig 拦截器
 * @description: MVC拦截器
 * @author: Linn
 * @create: 2019-12-19 21:06
 **/

import com.springcloud.interceptor.TokenInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;

@Slf4j
@Configuration
public class MvcConfig extends WebMvcConfigurationSupport{
    @Resource
    private TokenInterceptor tokenInterceptor;

    /**
     * 服务自己的拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有请求
        registry.addInterceptor(tokenInterceptor).addPathPatterns("/**")
        // 排除不需要拦截的请求地址
        .excludePathPatterns("/user/**");
        super.addInterceptors(registry);
    }

}
