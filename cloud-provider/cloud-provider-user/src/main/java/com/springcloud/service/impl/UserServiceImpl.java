package com.springcloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.springcloud.mapper.UserMapper;
import com.springcloud.model.po.User;
import com.springcloud.service.UserService;
import com.springcloud.entities.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @ClassName: UserServiceImpl
 * @author: Linn
 * @Date: 2019年8月19日22:50:05
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    @Resource
    UserMapper userMapper;


    @Override
    public R selectByName(String name) {
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("username",name);
        User user = userMapper.selectOne(qw);
        return R.ok(user);
    }
}
