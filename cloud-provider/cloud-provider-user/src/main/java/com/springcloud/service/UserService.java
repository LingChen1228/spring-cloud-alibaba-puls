package com.springcloud.service;

import com.springcloud.entities.R;
import org.apache.ibatis.annotations.Param;

public interface UserService {

    R selectByName(@Param("name") String name);
}
