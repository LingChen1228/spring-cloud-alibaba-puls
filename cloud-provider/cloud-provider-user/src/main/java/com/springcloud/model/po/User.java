package com.springcloud.model.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: User 用户表(User)实体类
 * @author: Linn
 * @Date: 2019年8月19日22:09:07
 */
@Data
@TableName(value = "t_user")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {
    private static final long serialVersionUID = 491195210677429789L;
    /**
     * 用户id
     */
    @TableId
    private Integer id;

    /**
     * 登录账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 性别(0-默认未知,1-男,2-女)
     */
    private Object sex;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}
