package com.springcloud.controller;

import com.springcloud.service.UserService;
import com.springcloud.entities.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 */
@RestController
public class UserController
{
    @Resource
    private UserService userService;


    @GetMapping(value = "/user/nacos/{name}")
    @ResponseBody
    public R getUser(@PathVariable("name") String name)
    {
        return userService.selectByName(name);
    }
}
